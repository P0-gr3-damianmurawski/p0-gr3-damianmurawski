package pl.edu.uwm.wmi.damianmurawski.labolatorium00.week2;

import java.util.Scanner;

public class Zadanie_1_1_f {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        int n;
        double wartosc = 0;
        System.out.println("Podaj n: ");
        n = input.nextInt();
        double liczba[];
        liczba = new double[n];
        for(int i=0; i<n ; i++){
            System.out.println("Podaj liczbe: ");
            liczba[i] = input.nextDouble();
        }
        System.out.println("\n");
        for(int i=0; i<n ; i++){
            wartosc=wartosc+Math.pow(liczba[i],2);
        }
        System.out.println(wartosc);
    }
}

