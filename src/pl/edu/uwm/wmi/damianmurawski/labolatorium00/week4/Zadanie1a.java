package pl.edu.uwm.wmi.damianmurawski.labolatorium00.week4;

public class Zadanie1a {
    public static void main(String[] args) {
        String string = "4cccc3aaa5CCCCCc";
        char znak = 'c';
        System.out.println("Ilosc wystapien znaku 'c': "+countChar(string,znak));

    }

    public static int countChar(String str,char c) {
        int wynik = 0;
        int dlugosc;
        dlugosc=str.length();
        for(int i = 0 ; i < dlugosc;i++){
            if(str.charAt(i)==c){
                wynik = wynik + 1;
            }
        }
        return wynik;
    }
}
