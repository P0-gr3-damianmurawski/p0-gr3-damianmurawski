package pl.edu.uwm.wmi.damianmurawski.labolatorium00.week6;
import pl.imiajd.murawski.Adres;
public class Zadanie2 {
    public static void main(String[] args) {
        Adres adres1 = new Adres("spokojna",5,4,"ciechanow","06-400");
        adres1.pokaz();
        Adres adres2 = new Adres("Spokojna",3,"ciechanow","06-500");
        adres2.pokaz();
        boolean x = adres2.przed(adres1);
        System.out.println(x);
    }
}
//class Adres {
//    private String ulica;
//    private int numer_domu;
//    private int numer_mieszkania;
//    private String miasto;
//    private String kod_pocztowy;
//    public Adres(String ulica1,int numer_domu1,int numer_mieszkania1,String miasto1,String kod_pocztowy1){
//        this.ulica = ulica1;
//        this.numer_domu=numer_domu1;
//        this.numer_mieszkania=numer_mieszkania1;
//        this.miasto=miasto1;
//        this.kod_pocztowy=kod_pocztowy1;
//    }
//    public Adres(String ulica2,int numer_domu2,String miasto2,String kod_pocztowy2){
//        this.ulica = ulica2;
//        this.numer_domu=numer_domu2;
//        this.miasto=miasto2;
//        this.kod_pocztowy=kod_pocztowy2;
//    }
//    void pokaz(){
//        System.out.println("Kod pocztowy: "+kod_pocztowy+" Miasto: "+miasto);
//        System.out.println("Numer domu: "+numer_domu+" Numer mieszkania: "+numer_mieszkania+" Ulica: "+ulica);
//    }
//    public boolean przed(Adres object){
//        if(kod_pocztowy.equals(object.kod_pocztowy)){
//            return true;
//        }else{
//            return false;
//        }
//    }
//}
