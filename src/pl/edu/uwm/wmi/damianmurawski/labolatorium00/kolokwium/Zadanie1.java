package pl.edu.uwm.wmi.damianmurawski.labolatorium00.kolokwium;
import java.util.Scanner;
public class Zadanie1 {
    public static void main(String[] args) {
        int n;
        Scanner input = new Scanner(System.in);
        System.out.println("Podaj n: ");
        n = input.nextInt();
        int[] tab = new int[n];
        for (int i = 0; i < n; i++) {
            System.out.println("Podaj liczbe calkowita: ");
            tab[i] = input.nextInt();
        }
        pierwsze(tab, n);
    }

    public static void pierwsze(int[] tab, int n) {
        for (int i = 0; i < tab.length; i++) {
            boolean pierwsza = true;
            if (tab[i] == 1) {
                pierwsza = false;
            } else {
                for (int j = 2; j <= tab[i] / 2; j++) {
                    if (tab[i]% j == 0) {
                        pierwsza = false;
                        break;
                    }

                }
                if (pierwsza)
                    System.out.println(tab[i]+"jest liczba pierwsza");
            }
        }
    }
}
