package pl.edu.uwm.wmi.damianmurawski.labolatorium00.week6;
import pl.imiajd.murawski.BetterRectangle;
public class Zadanie7 {
    public static void main(String[] args) {
        BetterRectangle prostakat = new BetterRectangle(6,10);
        System.out.println("Obwod: "+prostakat.getPerimeter());
        System.out.println("Pole powierzchni: "+prostakat.getArea());
    }

}
//class BetterRectangle extends java.awt.Rectangle{
//    public BetterRectangle(int height , int width){
//        super(height,width);
//    }
//    public double getPerimeter(){
//        double perimeter;
//        perimeter = 2*(height+width);
//        return perimeter;
//    }
//    public double getArea(){
//        double area;
//        area = height*width;
//        return area;
//    }
//}
