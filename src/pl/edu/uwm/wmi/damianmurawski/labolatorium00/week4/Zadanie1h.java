package pl.edu.uwm.wmi.damianmurawski.labolatorium00.week4;

import java.util.Scanner;

public class Zadanie1h {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        System.out.println("Podaj liczbe: ");
        String liczba = input.nextLine();
        System.out.println("Podaj znak: ");
        String znak = input.nextLine();
        System.out.println("Podaj co ile: ");
        int ile = input.nextInt();
        System.out.println(nice(liczba,ile,znak));
    }
    public static String nice(String str,int ile,String znak){
        StringBuffer msg = new StringBuffer(str);
        for(int i = str.length()-ile ; i > 0 ; i-=ile){
            msg.insert(i,znak);
        }
        str=msg.toString();
        return str;
    }
}

