package pl.edu.uwm.wmi.damianmurawski.labolatorium00.week2;

import java.util.Scanner;

public class Zadanie_1_1_d {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        int n;
        double wartosc = 0;
        System.out.println("Podaj n: ");
        n = input.nextInt();
        double liczba[];
        liczba = new double[n];
        for(int i=0; i<n ; i++){
            System.out.println("Podaj liczbe: ");
            liczba[i] = input.nextDouble();
        }
        System.out.println("\n");
        for(int i=0; i<n ; i++){
            liczba[i]=Math.sqrt(Math.abs(liczba[i]));
            wartosc=wartosc+liczba[i];
        }
        System.out.println(wartosc);
    }
}

