package pl.edu.uwm.wmi.damianmurawski.labolatorium00.week2;

import java.util.Scanner;

public class Zadanie_2_3 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        int n;
        int zera=0;
        int dodatnie=0;
        int ujemne=0;
        System.out.println("Podaj n: ");
        n = input.nextInt();
        double liczba[];
        liczba = new double[n];
        for(int i=0; i<n ; i++){
            System.out.println("Podaj liczbe: ");
            liczba[i] = input.nextDouble();
        }
        System.out.println("\n");
        for(int i=0; i<n ; i++) {
            if(liczba[i]>=0)
                dodatnie++;
            if(liczba[i]==0)
                zera++;
            if(liczba[i]<0)
                ujemne++;
        }
        System.out.println("Dodatnie: "+dodatnie);
        System.out.println("Zera: "+zera);
        System.out.println("Ujemne: "+ujemne);
    }
}