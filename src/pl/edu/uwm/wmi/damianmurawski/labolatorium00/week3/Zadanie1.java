package pl.edu.uwm.wmi.damianmurawski.labolatorium00.week3;

import java.util.Random;
import java.util.Scanner;

public class Zadanie1 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        int n;
        System.out.println("Podaj n: ");
        n = input.nextInt();
        if (n < 1 || n >= 101) {
            System.out.println("N = " + n + "\nN musi byc od 1 do 100");
            return;
        }
        int[] tab = new int[n];
        int min = -999;
        int max = 999;
        Random random = new Random();
        for(int j=0; j<n; j++){
            tab[j] = (random.nextInt(max - min )+min);
            System.out.println(tab[j]);
        }
        int nieparzyste =0;
        for(int i=0;i<n;i++){
            if(tab[i]%2!=0)
                nieparzyste++;
        }
        System.out.println("Ilość nieparzystch: "+nieparzyste);

        int parzyste =0;
        for(int i=0;i<n;i++){
            if(tab[i]%2==0)
                parzyste++;
        }
        System.out.println("Ilość parzystch: "+parzyste);

        int dodatnie =0;
        for(int i=0;i<n;i++){
            if(tab[i]>0)
                dodatnie++;
        }
        System.out.println("Ilość dodatnich: "+dodatnie);

        int zerowe =0;
        for(int i=0;i<n;i++){
            if(tab[i]==0)
                zerowe++;
        }
        System.out.println("Ilość zer: "+zerowe);

        int ujemne =0;
        for(int i=0;i<n;i++){
            if(tab[i]<0)
                ujemne++;
        }
        System.out.println("Ilość ujemnych: "+ujemne);

        int max1=-999;
        int licznik =0;
        for(int i=0; i<n;i++){
            if(max1<tab[i])
                max1=tab[i];
        }
        for(int i=0; i<n;i++){
            if(max1==tab[i])
                licznik++;
        }
        System.out.println("Ilość wystąpień maksymalnego elementu:  "+licznik);

        int sumaDodatnich = 0;
        for(int i=0;i<n;i++) {
            if (tab[i] > 0)
                sumaDodatnich += tab[i];
        }
        System.out.println("Suma dodatnich:  " + sumaDodatnich);

        int sumaUjemnych = 0;
        for(int i=0;i<n;i++){
            if(tab[i]<0)
                sumaUjemnych += tab[i];
        }
        System.out.println("Suma ujemnych:  " + sumaUjemnych);

        int iloscDodatnich = 0;
        int zmienna = 0;
        for(int i = 0; i<n; i++){
            if(tab[i]>0){
                zmienna++;
            }
            if(zmienna > iloscDodatnich){
                iloscDodatnich = zmienna;
            }
            if(tab[i]<0){
                zmienna = 0;
            }
        }
        System.out.println("Długość maksymalnego ciagu dodatnich: " + iloscDodatnich);

        int[] tab2 = new int[n];
        System.out.println("\nTablica po zamianie(signum):");
        for(int i=0 ; i<n;i++){
            if(tab[i]<0)
                tab2[i]=-1;
            if(tab[i]>0)
                tab2[i]=1;
            System.out.println(tab2[i]);
        }

        // podpunkt g)
        int lewy;
        int prawy;
        int[] temp = new int[n];
        System.out.println("Podaj lewy: ");
        lewy = input.nextInt();
        if(1>lewy || lewy>=n){
            System.out.println("Lewy musi być od <1,"+n+")");
            return;
        }
        System.out.println("Podaj prawy: ");
        prawy = input.nextInt();
        if(1>prawy || prawy>=n){
            System.out.println("Prawy musi być od <1,"+n+")");
            return;
        }
        if(lewy>=prawy){
            System.out.println("Lewy musi być < Prawy");
            return;
        }
        int k = 1;
        int j=0;
        System.out.println("Odwrócony fragment: ");
        for(int i = lewy; i<prawy ; i++){
            temp[j]=tab[prawy-k];
            System.out.print(" "+temp[j]);
            j++;
            k++;
        }
        System.out.println("\nPo odwróceniu: ");
        j =0;
        for(int i = lewy; i<prawy ; i++){
            tab[i]=temp[j];
            j++;
        }
        for(int i = 0 ; i<n;i++){
            System.out.print(" "+tab[i]);
        }
    }
}
