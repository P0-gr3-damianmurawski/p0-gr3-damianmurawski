package pl.edu.uwm.wmi.damianmurawski.labolatorium00;

public class Zadanie11 {
    public static void main(String[] args) {
       String wiersz =
                       "\n\n        Jan Kochanowski Tren VIII\n\n"+
                       "Wielkieś mi uczyniła pustki w domu moim,\n" +
                       "    Moja droga Orszulo, tym zniknienim swoim.\n" +
                       "Pełno nas, a jakoby nikogo nie było:\n" +
                       "    Jedną maluczką duszą tak wiele ubyło.\n" +
                       "Tyś za wszytki mówiła, za wszytki śpiewała,\n" +
                       "    Wszytkiś w domu kąciki zawżdy pobiegała.\n" +
                       "Nie dopuściłaś nigdy matce sie frasować\n" +
                       "    Ani ojcu myśleniem zbytnim głowy psować,\n" +
                       "To tego, to owego wdzięcznie obłapiając\n" +
                       "    I onym swym uciesznym śmiechem zabawiając.\n" +
                       "Teraz wszytko umilkło, szczere pustki w domu,\n" +
                       "    Nie masz zabawki, nie masz rośmiać sie nikomu.\n" +
                       "Z każdego kąta żałość człowieka ujmuje,\n" +
                       "    A serce swej pociechy darmo upatruje. \n"+
                       "";
        System.out.println(wiersz);
    }
}
