package pl.edu.uwm.wmi.damianmurawski.labolatorium00.week7;
import pl.imiajd.murawski.z08.zadanie3.*;


import java.time.LocalDate;
import java.util.ArrayList;

public class Zadanie3 {
    public static void main(String[] args) {
        ArrayList<Instrument> orkiestra = new ArrayList<>();
        orkiestra.add(new Flet("Yamaha", LocalDate.of(2020, 1, 12)));
        orkiestra.add(new Fortepian("Petrof", LocalDate.of(2019, 2, 11)));
        orkiestra.add(new Skrzypce("Yamaha", LocalDate.of(2018, 3, 10)));
        orkiestra.add(new Skrzypce("Yamaha", LocalDate.of(2017, 4, 9)));
        orkiestra.add(new Flet("Yamaha", LocalDate.of(2016, 5, 8)));
        for (Instrument i : orkiestra){
            System.out.println(i);i.dzwiek();

        }

    }
}
