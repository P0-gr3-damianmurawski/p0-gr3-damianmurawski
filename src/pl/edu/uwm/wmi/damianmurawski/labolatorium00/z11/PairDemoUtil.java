package pl.edu.uwm.wmi.damianmurawski.labolatorium00.z11;

public class PairDemoUtil {
    public static void main(String[] args)
    {
        Pair<String> para1 = new Pair<>("lewy","prawy");
        System.out.println(para1.getFirst());
        System.out.println(para1.getSecond());
        para1=PairUtil.swap(para1);
        System.out.println(para1.getFirst());
        System.out.println(para1.getSecond());
    }
}

