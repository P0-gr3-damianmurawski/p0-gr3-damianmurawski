package pl.edu.uwm.wmi.damianmurawski.labolatorium00.z12;

import java.util.LinkedList;

public class Zadanie4 {
    public static void main(String[] args) {
        LinkedList<String> pracownicy = new LinkedList<>();
        pracownicy.add("Kowalski");
        pracownicy.add("Nowak");
        pracownicy.add("Wisniewski");
        pracownicy.add("Drozda");
        pracownicy.add("Piotrowski");
        pracownicy.add("Ptaszkiewicz");
        for(String i:pracownicy){
            System.out.println(i);
        }
        odwroc(pracownicy);
        System.out.println("\nODWROC:\n");
        for(String i:pracownicy){
            System.out.println(i);
        }
    }
    public static<T> void odwroc(LinkedList<T> lista){
        LinkedList<T> pracownicy2 = new LinkedList<>(lista);
        int j=0;
        for(int i=lista.size()-1;i>=0;i--) {
            lista.set(i, pracownicy2.get(j));
            j++;
        }
    }
}
