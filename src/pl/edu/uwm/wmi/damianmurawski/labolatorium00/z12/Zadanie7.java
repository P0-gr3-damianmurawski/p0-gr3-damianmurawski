package pl.edu.uwm.wmi.damianmurawski.labolatorium00.z12;

import java.util.*;

public class Zadanie7 {
    public static void main(String[] args){
        Scanner scanner = new Scanner(System.in);
        System.out.print("Podaj liczbe: ");
        Integer n = scanner.nextInt();
        sitoEratostenesa(n);
    }
    public static void sitoEratostenesa(Integer n){
        HashSet<Integer> pierwsze = new HashSet<>();
        for(int i = 2; i < n; i++){
            pierwsze.add(i);
        }
        for(int i=2;i<=Math.sqrt(n);i++)
        {
            for(int t=i+i;t<=n;t=t+i){
                pierwsze.remove(t);
            }
        }
        for(Integer i : pierwsze){
            System.out.print(i + " ");
        }

    }
}
