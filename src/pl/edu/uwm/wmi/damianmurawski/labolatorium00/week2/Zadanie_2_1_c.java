package pl.edu.uwm.wmi.damianmurawski.labolatorium00.week2;

import java.util.Scanner;

public class Zadanie_2_1_c {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        int n;
        int wynik=0;
        System.out.println("Podaj n: ");
        n = input.nextInt();
        double liczba[];
        liczba = new double[n];
        for(int i=0; i<n ; i++){
            System.out.println("Podaj kolejna liczbe: ");
            liczba[i] = input.nextDouble();
        }
        System.out.println("\n");
        for(int i=0; i<n ; i++) {
            liczba[i]=Math.sqrt(liczba[i]);
            if(liczba[i]%2==0)
                wynik+=1;
        }
        System.out.println(wynik);
    }
}
