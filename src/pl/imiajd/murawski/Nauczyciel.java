package pl.imiajd.murawski;

public class Nauczyciel extends Osoba {
    double pensja;

    public Nauczyciel(String nazwisko, int rokUrodzenia, double pensja) {
        super(nazwisko, rokUrodzenia);
        this.pensja = pensja;
    }
    @Override
    public String toString() {
        return super.toString() + " Pensja: " + pensja;
    }

    public double getPensja() {
        return pensja;
    }
}