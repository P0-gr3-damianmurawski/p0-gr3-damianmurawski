package pl.edu.uwm.wmi.damianmurawski.labolatorium00.week5;

import java.util.ArrayList;

public class Zadanie4 {
    public static void main(String[] args) {
        ArrayList<Integer> arr1 = new ArrayList<Integer>(8);
        for (int i = 0; i < 8; i++) {
            arr1.add(i, i + 1);
        }
        System.out.println(arr1);
        System.out.println(reversed(arr1));

    }
    public static ArrayList<Integer> reversed(ArrayList<Integer> arr1){
        ArrayList<Integer> arr2 = new ArrayList<Integer>();
        int j =arr1.size()-1;
        for(int i=0;i<arr1.size();i++){
            arr2.add(i, arr1.get(j));
            j--;
        }
        return arr2;
    }
}
