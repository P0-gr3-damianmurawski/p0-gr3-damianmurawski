package pl.imiajd.murawski.z10;

import java.time.LocalDate;

public class Osoba implements Cloneable,Comparable<Osoba> {
    private String nazwisko;
    private LocalDate dataUrodzenia;
    public Osoba(String nazwisko , LocalDate dataUrodzenia){
        this.nazwisko=nazwisko;
        this.dataUrodzenia=dataUrodzenia;
    }
    @Override
    public String toString(){
        return nazwisko +" "+ dataUrodzenia;
    }
    @Override
    public boolean equals(Object obj) {
        Osoba temp = (Osoba) obj;
        if(this == obj){
            return true;
        }
        if(this.nazwisko.equals(temp.nazwisko) && this.dataUrodzenia.equals(temp.dataUrodzenia)) {
            return true;
        }else{
            return false;
    }
    }

    public int compareTo(Osoba obj) {
        int wynik;
        if (this.nazwisko.compareTo(obj.nazwisko) == 0) {
            wynik = this.dataUrodzenia.compareTo(obj.dataUrodzenia);
            return wynik;
        }
        wynik = this.nazwisko.compareTo(obj.nazwisko);
        return wynik;
    }


}
