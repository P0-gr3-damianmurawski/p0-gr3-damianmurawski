package pl.edu.uwm.wmi.damianmurawski.labolatorium00.week2;

import java.util.Scanner;

public class Zadanie_2_4 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        int n;
        double najmniejsza=0;
        double najwieksza=0;
        System.out.println("Podaj n: ");
        n = input.nextInt();
        double liczba[];
        liczba = new double[n];
        for(int i=0; i<n ; i++){
            System.out.println("Podaj kolejna liczbe: ");
            liczba[i] = input.nextDouble();
        }
        najmniejsza=liczba[0];
        System.out.println("\n");
        for(int i=0; i<n ; i++) {
            if(liczba[i]>najwieksza)
                najwieksza=liczba[i];
            if(liczba[i]<najmniejsza)
                najmniejsza=liczba[i];

        }
        System.out.println("Najemniejsza: "+najmniejsza);
        System.out.println("Najwieksza: "+najwieksza);
    }
}

