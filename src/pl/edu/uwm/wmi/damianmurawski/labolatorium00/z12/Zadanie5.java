package pl.edu.uwm.wmi.damianmurawski.labolatorium00.z12;

import java.util.Stack;

public class Zadanie5 {
    public static void main(String[] args) {
        Stack<String> stos = new Stack<>();

        String zdanie = "Ala ma kota. Jej kot lubi myszy.";
        System.out.println(zdanie);
        String[] strings = zdanie.split(" ");

        String end = "";

        for (int i = 0; i < strings.length; i++) {
            if (!strings[i].endsWith(".")) {
                strings[i] = Character.toLowerCase(strings[i].charAt(0)) + strings[i].substring(1);
                stos.push(strings[i]);
            } else {
                strings[i] = strings[i].substring(0, strings[i].length() - 1);
                stos.push(strings[i]);
                String temp = "";
                while (!stos.isEmpty()) {
                    temp += stos.pop() + " ";

                }
                temp = Character.toUpperCase(temp.charAt(0)) + temp.substring(1);
                temp = temp.substring(0, temp.length() - 1);
                end += temp + ". ";
            }

        }

        System.out.println(end);
    }
}
