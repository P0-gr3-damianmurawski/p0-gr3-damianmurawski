package pl.edu.uwm.wmi.damianmurawski.labolatorium00.z11;

import java.time.LocalDate;
import java.util.Arrays;

public class ArrayUtil {
    public static void main(String[] args) {
        // ZADANIE 3
        Integer[] array = {10, 10, 20, 20, 50, 30};
        for (Integer a : array) {
            System.out.print(a + ",");
        }
        System.out.println("    " + ArrayUtil.isSorted(array));
        System.out.println();
        Integer[] array2 = {113, 113, 122, 142, 155, 223};
        for (Integer a : array2) {
            System.out.print(a + ",");

        }
        System.out.println("     " + ArrayUtil.isSorted(array2));
        System.out.println();
        LocalDate[] array3 = new LocalDate[3];
        array3[0] = LocalDate.of(1901, 1, 3);
        array3[1] = LocalDate.of(1900, 1, 2);
        array3[2] = LocalDate.of(1800, 1, 4);
        for (LocalDate a : array3) {
            System.out.println(a);
        }
        System.out.println("\n" + ArrayUtil.isSorted(array3));
        System.out.println();
        LocalDate[] array4 = new LocalDate[5];
        array4[0] = LocalDate.of(1900, 10, 3);
        array4[1] = LocalDate.of(1910, 10, 1);
        array4[2] = LocalDate.of(1920, 10, 1);
        array4[3] = LocalDate.of(1920, 10, 1);
        array4[4] = LocalDate.of(1930, 10, 1);
        for (LocalDate a : array4) {
            System.out.println(a);
        }
        System.out.println("\n" + ArrayUtil.isSorted(array4));
        System.out.println();
        LocalDate[] array5 = new LocalDate[2];
        array5[0] = LocalDate.of(1900, 10, 3);
        array5[1] = LocalDate.of(2002, 10, 1);
        //ZADANIE 4
        System.out.println("BIN SEARCH LOCAL DATE:" + ArrayUtil.binSearch(array4, array5[0]));
        System.out.println("BIN SEARCH INTEGER:" +ArrayUtil.binSearch(array2,array[5]));
        //ZADANIE 5
        System.out.println("SELECTION SORT LOCAL DATE:");
        ArrayUtil.selectionSort(array3);
        for (LocalDate a : array3) {
            System.out.println(a);
        }
        System.out.println("SELECTION SORT INTEGER:");
        ArrayUtil.selectionSort(array);
        for (Integer a : array) {
            System.out.print(a+",");
        }
        //ZADANIE 6
        System.out.println("\nMERGE SORT INTEGER:");
        ArrayUtil.mergeSort(array2);
        for (Integer a : array2) {
            System.out.print(a+",");
        }
        System.out.println("\nMERGE SORT LOCAL DATE");
        ArrayUtil.mergeSort(array3);
        for (LocalDate a : array3) {
            System.out.println(a);
        }

    }

    public static <T extends Comparable<T>> String isSorted(T[] object) {
        boolean temp = true;
        for (int i = 1; i < object.length; i++) {
            temp = object[i - 1].compareTo(object[i]) > 0;
        }
        if (temp)
            return "Ciąg nie jest uporzadkowany niemalejaca";
        else
            return "Ciąg jest uporzadkowany niemalejaca";
    }

    public static <T extends Comparable<T>> int binSearch(T[] object, T search) {
        {
            int l = 0, r = object.length - 1;
            while (l <= r) {
                int m = l + (r - l) / 2;
                if (object[m].compareTo(search) == 0)
                    return m;
                if (object[m].compareTo(search) < 0)
                    l = m + 1;
                else
                    r = m - 1;
            }
            return -1;
        }
    }

    public static <T extends Comparable<T>> void selectionSort(T[] object) {
        int max = object.length;
        for (int i = 0; i < max-1; i++) {
            int min = i;
            for (int j = i; j < max; j++) {
                if (object[min].compareTo(object[j]) > 0) {
                    min = j;
                }
            }
            T temp = object[i];
            object[i] = object[min];
            object[min] = temp;
        }
    }
    public static <T extends Comparable<T>> void mergeSort(T[] object){
        if (object.length <= 1) return;
        int srodek = object.length / 2;
        T[] left = Arrays.copyOfRange(object, 0, srodek);
        T[] right = Arrays.copyOfRange(object, srodek, object.length);
        mergeSort(left);
        mergeSort(right);
        int i = 0, j = 0, k = 0;
        while (i < left.length && j < right.length) {
            if (left[i].compareTo(right[j]) < 0) {
                object[k] = left[i];
                i++;
            } else {
                object[k] = right[j];
                j++;
            }
            k++;
        }
        while (i < left.length) {
            object[k] = left[i];
            i++;
            k++;
        }
        while (j < right.length) {
            object[k] = right[j];
            j++;
            k++;
        }
    }
}

