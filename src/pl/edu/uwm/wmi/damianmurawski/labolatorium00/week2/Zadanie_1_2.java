package pl.edu.uwm.wmi.damianmurawski.labolatorium00.week2;

import java.util.Scanner;

public class Zadanie_1_2 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        int n;
        System.out.println("Podaj n: ");
        n = input.nextInt();
        int liczba[];
        liczba = new int[n];
        for(int i=0; i<n ; i++){
            System.out.println("Podaj liczbe: ");
            liczba[i] = input.nextInt();
        }
        System.out.println("\n");
        for(int i=1; i<n ; i++) {
            System.out.print(" "+liczba[i]);
        }
        System.out.print(" "+liczba[0]);
    }
}
