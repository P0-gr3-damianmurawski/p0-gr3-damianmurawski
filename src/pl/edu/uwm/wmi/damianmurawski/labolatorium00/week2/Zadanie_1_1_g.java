package pl.edu.uwm.wmi.damianmurawski.labolatorium00.week2;

import java.util.Scanner;

public class Zadanie_1_1_g {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        int n;
        int wartosc = 0;
        int wartosc1 = 0;
        int wartosc2 = 1;
        System.out.println("Podaj n: ");
        n = input.nextInt();
        int liczba[];
        liczba = new int[n];
        for(int i=0; i<n ; i++){
            System.out.println("Podaj liczbe: ");
            liczba[i] = input.nextInt();
        }
        System.out.println("\n");
        for(int i=0; i<n ; i++){
            wartosc1+=liczba[i];
            wartosc2*=liczba[i];
        }
        System.out.println(wartosc1);
        System.out.println(wartosc2);
    }
}
