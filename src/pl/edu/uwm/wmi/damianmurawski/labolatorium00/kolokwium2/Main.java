package pl.edu.uwm.wmi.damianmurawski.labolatorium00.kolokwium2;

import java.util.*;

public class Main {
    public static void main(String[] args) {
        ArrayList <Kandydet> grupa = new ArrayList<>();
        grupa.add(new Kandydet("Kowalski",47,"licencjat",1));
        grupa.add(new Kandydet("Szeregowy",47,"mistrz",10));
        grupa.add(new Kandydet("Nowak",49,"mistrz",10));
        grupa.add(new Kandydet("Drozda",48,"mistrz",10));
        grupa.add(new Kandydet("Einstein",50,"mistrz",1));
        grupa.add(new Kandydet("Murawski",50,"mistrz",12));
        Rekrutacja.setDoswiadczenie();
        System.out.println("Sprawdzanie czy qualified  "+grupa.get(1)+"\nQualified: "+qualified(grupa.get(1)));

        System.out.println("lista:\n");
        Collections.shuffle(grupa);
        for(Kandydet i : grupa) {
            System.out.println(i);
        }
        Collections.sort(grupa);
        System.out.println("\nlista po sortowaniu:");
        for(Kandydet i : grupa) {
            System.out.println(i);
        }
        System.out.println("==================\nRecruitmentMap");
        RecruitmentMap(grupa);
    }
    public static boolean qualified(Kandydet k){
        if(k.getLatadoswiadczenia()>=Rekrutacja.doswiadczenie){
            return true;
        }else{
            return false;
        }
    }
    public static void RecruitmentMap(ArrayList<Kandydet> klist){
        Map<String, Integer> mapa = new TreeMap<>();;
        for(int i=0;i<klist.size();i++) {
            if (qualified(klist.get(i)) == true) {
                mapa.put(klist.get(i).getNazwa(),klist.get(i).getLatadoswiadczenia());
            }
        }
        System.out.println(mapa);

    }

}
