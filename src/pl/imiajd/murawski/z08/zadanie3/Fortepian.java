package pl.imiajd.murawski.z08.zadanie3;

import java.time.LocalDate;

public class Fortepian extends Instrument{
    @Override
    public void dzwiek() {
        System.out.println("Fortepian: PLIM, PLAM");
    }
    public Fortepian(String producent , LocalDate rokProdukcji){
        super(producent,rokProdukcji);
    }
}