package pl.imiajd.murawski.z08.zadanie3;

import java.time.LocalDate;

public abstract class Instrument {
   private String producent;
   private LocalDate rokProdukcji;

   public Instrument(String producent,LocalDate rokProdukcji){
       this.producent=producent;
       this.rokProdukcji=rokProdukcji;
   }
   public abstract void dzwiek();

    public String getProducent() {
        return producent;
    }

    public LocalDate getRokProdukcji() {
        return rokProdukcji;
    }
    @Override
    public boolean equals(Object obj){
        Instrument temp = (Instrument) obj;
        if(this == obj){
            return true;
        }
        if(this.producent.equals(temp.producent)&& this.rokProdukcji==temp.rokProdukcji){
            return true;
        }else{
            return false;
        }
    }
    @Override
    public String toString() {
        return  producent + " " + rokProdukcji;
    }
}

