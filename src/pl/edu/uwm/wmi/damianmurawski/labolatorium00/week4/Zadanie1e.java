package pl.edu.uwm.wmi.damianmurawski.labolatorium00.week4;

import java.util.Scanner;

public class Zadanie1e {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        System.out.println("Podaj 1 napis: ");
        String napis1 = input.nextLine();
        System.out.println("Podaj 2 napis: ");
        String napis2 = input.nextLine();
        int[] zadanie = where(napis1,napis2);
        for(int element: zadanie){
            if(element == 0){
                break;
            }
            System.out.println(element);

        }
    }
    public static int[] where(String str, String subStr) {
        int[] tab = new int[str.length()];
        int index = 0;
        for (int i = -1; (i = str.indexOf(subStr, i + 1)) != -1; i++) {
            tab[index] = i + 1;
            index++;
        }
        return tab;
    }
}
