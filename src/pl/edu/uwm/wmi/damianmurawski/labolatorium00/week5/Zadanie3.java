package pl.edu.uwm.wmi.damianmurawski.labolatorium00.week5;

import java.util.ArrayList;

public class Zadanie3 {
    public static void main(String[] args) {
        ArrayList<Integer> arr1 = new ArrayList<Integer>(8);
        for (int i = 0; i < 8; i++) {
            arr1.add(i, i + 1);
        }
        ArrayList<Integer> arr2 = new ArrayList<Integer>(3);
        for (int i = 0; i < 3; i++) {
            arr2.add(i,  i + 1);
        }
        System.out.println(arr1);
        System.out.println(arr2);
        System.out.println(mergeSorted(arr1,arr2));
    }
    public static ArrayList<Integer> mergeSorted(ArrayList<Integer> arr1, ArrayList<Integer> arr2){
        for (int index1 = 0, index2 = 0; index2 < arr2.size(); index1++) {
            if (index1 == arr1.size() || arr1.get(index1) > arr2.get(index2)) {
                arr1.add(index1, arr2.get(index2++));
            }
        }
        return arr1;
    }
}
