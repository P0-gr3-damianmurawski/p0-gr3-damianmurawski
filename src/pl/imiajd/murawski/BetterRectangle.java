package pl.imiajd.murawski;

public class BetterRectangle extends java.awt.Rectangle {
    public BetterRectangle(int height , int width){
        super(height,width);
    }
    public double getPerimeter(){
        double perimeter;
        perimeter = 2*(height+width);
        return perimeter;
    }
    public double getArea(){
        double area;
        area = height*width;
        return area;
    }
}
