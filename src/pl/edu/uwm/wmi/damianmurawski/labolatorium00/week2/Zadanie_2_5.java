package pl.edu.uwm.wmi.damianmurawski.labolatorium00.week2;
import java.util.Scanner;
import java.lang.Math;
public class Zadanie_2_5 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        int n;
        System.out.println("Podaj n: ");
        n = input.nextInt();
        int liczby[];
        liczby = new int[n];
        for (int i = 0; i <n; i++) {
            System.out.println("Podaj liczbe: ");
            liczby[i] = input.nextInt();
        }
        for (int i = 0; i <n-1; i++) {
            if(liczby[i]<0 && liczby[i+1]<0)
                System.out.print("("+liczby[i]+","+liczby[i+1]+")");
            if(liczby[i]>0 && liczby[i+1]>0)
                System.out.print("("+liczby[i]+","+liczby[i+1]+")");

        }
    }
}
