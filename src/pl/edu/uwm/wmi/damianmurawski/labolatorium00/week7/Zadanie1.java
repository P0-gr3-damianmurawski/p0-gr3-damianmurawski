package pl.edu.uwm.wmi.damianmurawski.labolatorium00.week7;
import pl.imiajd.murawski.z08.Osoba;
import pl.imiajd.murawski.z08.Student;
import pl.imiajd.murawski.z08.Pracownik;

public class Zadanie1 {
    public static void main(String[] args) {
        Osoba[] ludzie = new Osoba[2];

        ludzie[0] = new Pracownik("Jan Kowalski",new String[] {"Jan","kuba"} ,50000,false,1991,2,10,2015,12,12);
        ludzie[1] = new Student("Małgorzata Nowak",new String[]{"Malgorzata"}, "informatyka",true,3.5,1922,7,1);

        for (Osoba p : ludzie) {
            System.out.println(p.getNazwisko() + ": \nImiona: "+ p.getImiona()+"\nPlec: "+p.getplec()+"\n" + p.getOpis()+"\n"+p.getDataUrodzenia()+"\n");
        }
        Student karol = new Student("Małgorzata Nowak",new String[]{"Malgorzata"}, "informatyka",true,3.5,1999,12,1);
        System.out.println(karol.getsredniaOcen());
        karol.setsredniaocen(3.0);
        System.out.println(karol.getsredniaOcen());
        Pracownik jan = new Pracownik("Jan Kowalski",new String[] {"Jan","kuba"} ,50000,false,1991,2,10,2015,12,12);
        System.out.println(jan.getDataZatrudnienia());
    }
}

//abstract class Osoba
//    {
//        public Osoba(String nazwisko,String imiona,boolean plec,int year, int month, int day)
//        {
//            this.nazwisko = nazwisko;
//            this.imiona[0] = imiona;
//            this.plec=plec;
//            this.dataUrodzenia=LocalDate.of(year,month,day);
//        }
//
//        public abstract String getOpis();
//
//
//        public String getNazwisko()
//        {
//            return nazwisko;
//        }
//        public LocalDate getDataUrodzenia()
//        {
//            return dataUrodzenia;
//        }
//        public String getImiona()
//        {
//            return imiona[0];
//        }
//        public String plec()
//        {
//            if (plec == true){
//                return "Kobieta";
//            }else{
//                return "Mężczyzna";
//            }
//        }
//
//        private String nazwisko;
//        private String[] imiona = new String[100];
//        private boolean plec;
//        private LocalDate dataUrodzenia;
//    }
//
//    class Pracownik extends Osoba
//    {
//        public Pracownik(String nazwisko,String imiona, double pobory,boolean plec,int year1, int month1, int day1,int year, int month, int day)
//        {
//            super(nazwisko,imiona,plec,year,month,day);
//            this.pobory = pobory;
//            this.dataZatrudnienia=LocalDate.of(year1,month1,day1);
//        }
//
//        public double getPobory()
//        {
//            return pobory;
//        }
//
//        public String getOpis()
//        {
//            return String.format("pracownik z pensją %.2f zł", pobory);
//        }
//
//        private double pobory;
//        private LocalDate dataZatrudnienia;
//        public String getDataZatrudnienia() {
//            return "Data zatrudnienia: " + dataZatrudnienia;
//        }
//
//    }
//
//
//    class Student extends Osoba {
//        public Student(String nazwisko,String imiona, String kierunek,boolean plec,double średniaOcen,int year, int month, int day) {
//            super(nazwisko,imiona,plec,year,month,day);
//            this.kierunek = kierunek;
//            this.średniaOcen=średniaOcen;
//        }
//
//        public String getOpis() {
//            return "kierunek studiów: " + kierunek + "\nsrednia ocen: "+średniaOcen;
//        }
//        public void setsredniaocen(double srednia){
//            this.średniaOcen=srednia;
//        }
//        public String getsredniaOcen() {
//            return "Srednia ocen: " + średniaOcen;
//        }
//        private String kierunek;
//        private double średniaOcen;
//    }
//
//
