package pl.imiajd.murawski.z08;

import java.time.LocalDate;

public class Pracownik extends Osoba {
    public Pracownik(String nazwisko, String[] imiona, double pobory, boolean plec, int year1, int month1, int day1, int year, int month, int day)
    {
        super(nazwisko,imiona,plec,year,month,day);
        this.pobory = pobory;
        this.dataZatrudnienia=LocalDate.of(year1,month1,day1);
    }

    public double getPobory()
    {
        return pobory;
    }

    public String getOpis()
    {
        return String.format("pracownik z pensją %.2f zł", pobory);
    }

    private double pobory;
    private LocalDate dataZatrudnienia;
    public String getDataZatrudnienia() {
        return "Data zatrudnienia: " + dataZatrudnienia;
    }

}