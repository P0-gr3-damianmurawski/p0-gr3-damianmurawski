package pl.edu.uwm.wmi.damianmurawski.labolatorium00.week4;

import java.math.BigDecimal;
import java.util.Scanner;
import java.math.*;

public class Zadanie5 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Podaj K: ");
        BigDecimal KBig = scanner.nextBigDecimal();
        System.out.println("Podaj p: ");
        BigDecimal pBig = scanner.nextBigDecimal();
        System.out.println("Podaj n: ");
        BigDecimal nBig = scanner.nextBigDecimal();
        suma(KBig,pBig,nBig);
    }

    public static void suma(BigDecimal KBig,BigDecimal pBig,BigDecimal nBig) {
        int potegaINT = nBig.intValue();
        BigDecimal wynikKoncowy = pBig.divide(BigDecimal.valueOf(100)).add(BigDecimal.valueOf(1)).pow(potegaINT).multiply(KBig);
        System.out.println(wynikKoncowy.setScale(2, RoundingMode.CEILING));



    }
}
