package pl.edu.uwm.wmi.damianmurawski.labolatorium00.week6;

public class Zadanie6 {
    public static void main(String[] args) {
        BetterRectangle prostakat = new BetterRectangle(6,10);
        System.out.println("Obwod: "+prostakat.getPerimeter());
        System.out.println("Pole powierzchni: "+prostakat.getArea());
    }

}
class BetterRectangle extends java.awt.Rectangle{
    public BetterRectangle(int height, int width){
        this.setSize(height,width);
    }
    public double getPerimeter(){
        double perimeter;
        perimeter = 2*(height+width);
        return perimeter;
    }
    public double getArea(){
        double area;
        area = height*width;
        return area;
    }
}

