package pl.edu.uwm.wmi.damianmurawski.labolatorium00.week2;
import java.util.Scanner;
import java.lang.Math;
public class Zadanie_2_1_e {
    public static void main (String[]args){
        Scanner input = new Scanner(System.in);
        int n;
        int silnia = 1;
        int wynik = 0;
        int j=1;
        System.out.println("Podaj n: ");
        n = input.nextInt();
        int liczba[];
        liczba = new int[n];
        for (int i = 0; i < n; i++) {
            System.out.println("Podaj liczbe: ");
            liczba[i] = input.nextInt();
        }
        System.out.println("\n");
        for (int i = 1; i <= n; i++) {

            silnia=silnia*j;
            j++;
            if(Math.pow(2,i)<liczba[i-1] && liczba[i-1]< silnia) {
                wynik++;
            }
        }
        System.out.println(wynik);
    }
}
