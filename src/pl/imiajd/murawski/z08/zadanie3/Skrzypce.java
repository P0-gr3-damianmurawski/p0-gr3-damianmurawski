package pl.imiajd.murawski.z08.zadanie3;

import java.time.LocalDate;

public class Skrzypce extends Instrument{
    @Override
    public void dzwiek() {
        System.out.println("Skrzypce:li, li, li;");
    }
    public Skrzypce(String producent, LocalDate rokProdkucji){
        super(producent,rokProdkucji);
    }
}

