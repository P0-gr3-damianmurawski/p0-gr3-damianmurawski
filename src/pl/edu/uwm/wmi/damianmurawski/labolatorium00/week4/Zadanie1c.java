package pl.edu.uwm.wmi.damianmurawski.labolatorium00.week4;

import java.util.Scanner;

public class Zadanie1c {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        System.out.println("Podaj 1 napis: ");
        String napis1 = input.nextLine();
        System.out.println(middle(napis1));
    }
    public static String middle(String str){
        String temp;
        if(str.length() <=1){
            return "Napis musi miec conajmnniej 2 znaki";
        }
        else{
            int m1 = str.length()/2;
            if(str.length()%2==0)
                temp = str.substring(m1-1, m1+1);
            else
                temp = str.substring(m1, m1+1);
        }
        return temp;
    }
}
