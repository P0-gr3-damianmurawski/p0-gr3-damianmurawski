package pl.edu.uwm.wmi.damianmurawski.labolatorium00.week4;
public class Zadanie1d {
    public static void main(String[] args) {
        System.out.println("Wynik: "+repeat("Ho",4));
    }
    public static String repeat(String str, int n){
        String concat = str;
        for(int i=0 ;i<n-1;i++){
            concat += str;
        }
        return concat;
    }
}
