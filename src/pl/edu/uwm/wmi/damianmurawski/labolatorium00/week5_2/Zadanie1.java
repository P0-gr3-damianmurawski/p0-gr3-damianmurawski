package pl.edu.uwm.wmi.damianmurawski.labolatorium00.week5_2;

public class Zadanie1 {
    public static void main(String[] args) {
        RachunekBankowy saver1 = new RachunekBankowy(2000);
        RachunekBankowy saver2 = new RachunekBankowy(3000);
        RachunekBankowy.setRocznaStopaProcentowa(0.04);
        System.out.println(saver1.obliczMiesieczneOdsetki());
        System.out.println(saver2.obliczMiesieczneOdsetki());
        RachunekBankowy.setRocznaStopaProcentowa(0.05);
        System.out.println(saver1.obliczMiesieczneOdsetki());
        System.out.println(saver2.obliczMiesieczneOdsetki());

    }
}
class RachunekBankowy {
    static double rocznaStopaProcentowa;
    private double saldo;
    RachunekBankowy(double saldo){
        this.saldo = saldo;
    }
    double obliczMiesieczneOdsetki () {
        double odsetki;
        odsetki = (saldo * rocznaStopaProcentowa) / 12;
        saldo = saldo + odsetki;
        return saldo;
    }
    public static void setRocznaStopaProcentowa ( double stopaProcentowa){
        rocznaStopaProcentowa = stopaProcentowa;
    }
}
