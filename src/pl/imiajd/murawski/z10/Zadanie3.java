package pl.imiajd.murawski.z10;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Scanner;

public class Zadanie3 {
    public static void main(String[] args) throws FileNotFoundException {
        ArrayList<String> arrayList = new ArrayList<>();
        Scanner scanner_pliki = new Scanner(new File("D:/Studia/JavaProgramowanie/src/pl/imiajd/murawski/z10/test.txt"));
        while (scanner_pliki.hasNextLine()) {
            arrayList.add(scanner_pliki.nextLine());
        }
        for (String i : arrayList){
            System.out.println(i);
        }
        Collections.sort(arrayList);
        System.out.println("\nPo sortowaniu: \n");
        for (String i : arrayList){
            System.out.println(i);
        }
    }
}
