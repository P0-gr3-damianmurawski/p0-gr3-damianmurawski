package pl.imiajd.murawski;

public class Student extends Osoba {
    String kierunek;
    public Student(String nazwisko,int rokUrodzenia,String kierunek){
        super(nazwisko,rokUrodzenia);
        this.kierunek=kierunek;
    }
    @Override
    public String toString(){
        return super.toString() + " Kierunek: " + kierunek;
    }
    public String getKierunek(){
        return kierunek;
    }
}