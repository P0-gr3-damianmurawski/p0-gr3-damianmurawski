package pl.edu.uwm.wmi.damianmurawski.labolatorium00.z13;

import java.util.Map;
import java.util.Scanner;
import java.util.TreeMap;

public class Zadanie2 {
    private static final Map<String, String> map = new TreeMap<>(String::compareTo);

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        while (true) {
            System.out.print("Wybierz jedno:\n'dodaj','usun','pokaz','zmien','zakoncz'\n");
            String polecenie = scanner.nextLine();
            if (polecenie.equals("dodaj")) {
                System.out.print("Nazwisko: ");
                String nazwisko = scanner.nextLine();
                System.out.print("Ocena: ");
                String ocena = scanner.nextLine();
                map.put(nazwisko, ocena);
            }
            if (polecenie.equals("usun")) {
                System.out.print("Nazwisko do usuneicia: ");
                String nazwisko = scanner.nextLine();
                map.remove(nazwisko);
            }
            if (polecenie.equals("pokaz")) {
                System.out.println("LISTA:");
                for (Map.Entry<String, String> o : map.entrySet()) {
                    System.out.println(o.getKey() + ": " + o.getValue());
                }
            }
            if (polecenie.equals("zmien")) {
                System.out.print("Nazwisko do zmiany: ");
                String nazwisko = scanner.nextLine();
                System.out.print("Ocena: ");
                String ocena = scanner.nextLine();
                map.put(nazwisko, ocena);
            }
            if (polecenie.equals("zakoncz")) {
                break;
            }
        }
    }
}
