package pl.edu.uwm.wmi.damianmurawski.labolatorium00.week6;
import pl.imiajd.murawski.Osoba;
import pl.imiajd.murawski.Student;
import pl.imiajd.murawski.Nauczyciel;
public class Zadanie4 {
    public static void main(String[] args) {
        Osoba karol= new Osoba("kowalski",1994);
        Student wojtek = new Student("nowak",1999,"informatyka");
        Nauczyciel pawel = new Nauczyciel("Sparrow",1985,30000);
        System.out.println(pawel.getPensja());
        System.out.println(pawel.getNazwisko());
        System.out.println(pawel.getRokUrodzenia());
        System.out.println(pawel.toString());
        System.out.println(wojtek.getKierunek());
        System.out.println(wojtek.getNazwisko());
        System.out.println(wojtek.getRokUrodzenia());
        System.out.println(wojtek.toString());
        System.out.println(karol.getNazwisko());
        System.out.println(karol.getRokUrodzenia());
        System.out.println(karol.toString());
    }
}
//class Osoba{
//    private String nazwisko;
//    private int rokUrodzenia;
//    public Osoba(String nazwisko,int rokUrodzenia){
//        this.nazwisko=nazwisko;
//        this.rokUrodzenia=rokUrodzenia;
//    }
//    public String toString(){
//        return  (" Nazwisko: "+nazwisko+" Rok urodzenia: "+rokUrodzenia);
//    }
//    public String getNazwisko(){
//        return nazwisko;
//    }
//    public int getRokUrodzenia(){
//        return rokUrodzenia;
//    }
//}
//class Student extends  Osoba{
//    String kierunek;
//    public Student(String nazwisko,int rokUrodzenia,String kierunek){
//        super(nazwisko,rokUrodzenia);
//        this.kierunek=kierunek;
//    }
//    public String toString(){
//        return super.toString() + " Kierunek: " + kierunek;
//    }
//    public String getKierunek(){
//        return kierunek;
//    }
//}
//
//class Nauczyciel extends Osoba{
//    double pensja;
//    public Nauczyciel(String nazwisko,int rokUrodzenia,double pensja){
//        super(nazwisko,rokUrodzenia);
//        this.pensja=pensja;
//    }
//    public String toString(){
//        return super.toString() + " Pensja: " + pensja;
//    }
//    public double getPensja(){
//        return pensja;
//    }
//}
