package pl.edu.uwm.wmi.damianmurawski.labolatorium00.kolokwium2;

public class Kandydet implements Comparable<Kandydet> , Cloneable {
    private String nazwa;
    private int wiek;
    private String wyksztalcony;
    private int latadoswiadczenia;

    public Kandydet(String nazwa, int wiek, String wyksztalcony, int latadoswiadczenia) {
        this.nazwa = nazwa;
        this.wiek = wiek;
        this.wyksztalcony = wyksztalcony;
        this.latadoswiadczenia = latadoswiadczenia;
    }

    public String getNazwa() {
        return nazwa;
    }

    public int getWiek() {
        return wiek;
    }

    public String getWyksztalcony() {
        return wyksztalcony;
    }

    public int getLatadoswiadczenia() {
        return latadoswiadczenia;
    }

    @Override
    public String toString() {
        return "Kandydet{" +
                "nazwa='" + nazwa + '\'' +
                ", wiek=" + wiek +
                ", wyksztalcony='" + wyksztalcony + '\'' +
                ", latadoswiadczenia=" + latadoswiadczenia +
                '}';
    }

    @Override
    public int compareTo(Kandydet o) {
        if (this.wyksztalcony.compareTo(o.wyksztalcony) == 0) {
            if (this.wiek > o.wiek) {
                return 1;
            }
            if (this.wiek < o.wiek) {
                return -1;
            }
            if (this.wiek == o.wiek) {
                if (this.latadoswiadczenia > o.latadoswiadczenia) {
                    return 1;
                }
                if (this.latadoswiadczenia < o.latadoswiadczenia) {
                    return -1;
                }
                if (this.latadoswiadczenia == o.latadoswiadczenia) {
                    return 0;
                }
                return 0;
            }
        }return this.wyksztalcony.compareTo(o.wyksztalcony);
    }

}

