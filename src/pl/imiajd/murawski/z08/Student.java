package pl.imiajd.murawski.z08;

public class Student extends Osoba {
    public Student(String nazwisko, String[] imiona, String kierunek, boolean plec, double średniaOcen, int year, int month, int day) {
        super(nazwisko,imiona,plec,year,month,day);
        this.kierunek = kierunek;
        this.średniaOcen=średniaOcen;
    }

    public String getOpis() {
        return "kierunek studiów: " + kierunek + "\nsrednia ocen: "+średniaOcen;
    }
    public void setsredniaocen(double srednia){
        this.średniaOcen=srednia;
    }
    public String getsredniaOcen() {
        return "Srednia ocen: " + średniaOcen;
    }
    private String kierunek;
    private double średniaOcen;
}