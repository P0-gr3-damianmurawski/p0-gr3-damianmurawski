package pl.edu.uwm.wmi.damianmurawski.labolatorium00.z13;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Scanner;

public class Zadanie4 {
    public static void main(String[] args) throws FileNotFoundException {
        File plik = new File("D:\\Studia\\JavaProgramowanie\\src\\pl\\edu\\uwm\\wmi\\damianmurawski\\labolatorium00\\z13\\plik.txt");
        func(plik);
    }

    public static void func(File plik) throws FileNotFoundException {
        Scanner scanner = new Scanner(plik);
        Map<Integer, HashSet<String>> mapa = new HashMap<>();

        while(scanner.hasNext()){
            String slowo = scanner.next();
            Integer h = slowo.hashCode();
            if(mapa.containsKey(h)){
                mapa.get(h).add(slowo);
            } else {
                HashSet<String> temp = new HashSet<>();
                temp.add(slowo);
                mapa.put(h, temp);
            }

        }
        scanner.close();
        for(Map.Entry<Integer, HashSet<String>> map : mapa.entrySet()){
            if(map.getValue().size() > 1){
                for (String s : map.getValue()) {
                    System.out.println("[hashCode: " + map.getKey() + " slowo: " + s + "]");
                }
            }
        }
    }
}
