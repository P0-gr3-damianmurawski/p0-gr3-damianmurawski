package pl.edu.uwm.wmi.damianmurawski.labolatorium00.week4;

import java.util.Scanner;

public class Zadanie1b {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        System.out.println("Podaj 1 napis: ");
        String napis1 = input.nextLine();
        System.out.println("Podaj 2 napis: ");
        String napis2 = input.nextLine();
        System.out.println(countSubStr(napis1,napis2));

    }
    public static int countSubStr(String str, String subStr){
        int ilosc =0;
        String [] split = str.split(" ");
        for(int i=0 ; i<split.length; i++){
            if(split[i].equals(subStr))
                ilosc++;
        }
        return ilosc;

    }
}
