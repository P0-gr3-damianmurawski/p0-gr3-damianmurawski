package pl.edu.uwm.wmi.damianmurawski.labolatorium00.week5;

import java.util.ArrayList;

public class Zadanie2 {
    public static void main(String[] args) {
        ArrayList<Integer> arr1 = new ArrayList<Integer>(8);
        for (int i = 0; i < 8; i++) {
            arr1.add(i, i + 1);
        }
        ArrayList<Integer> arr2 = new ArrayList<Integer>(3);
        for (int i = 0; i < 7; i++) {
            arr2.add(i, arr1.size() + i + 1);
        }
        System.out.println(arr1);
        System.out.println(arr2);
        System.out.println(merge(arr1, arr2));
    }

    public static ArrayList<Integer> merge(ArrayList<Integer> arr1, ArrayList<Integer> arr2) {
        ArrayList<Integer> arrL = new ArrayList<Integer>();
        int dlugosc1 = arr1.size();
        int dlugosc2 = arr2.size();
        int j = 0;
        if (dlugosc1 > dlugosc2) {
            for (int i = 0; i < dlugosc2; i++) {
                arrL.add(j, arr1.get(i));
                j++;
                arrL.add(j, arr2.get(i));
                j++;
            }
            j = dlugosc2;
            for (int i = dlugosc2 * 2; i < dlugosc2 + dlugosc1; i++) {
                arrL.add(i, arr1.get(j));
                j++;

            }
        } else {
            for (int i = 0; i < dlugosc1; i++) {
                arrL.add(j, arr2.get(i));
                j++;
                arrL.add(j, arr1.get(i));
                j++;
            }
            j = dlugosc1;
            for (int i = dlugosc1 * 2; i < dlugosc1 + dlugosc2; i++) {
                arrL.add(i, arr2.get(j));
                j++;

            }

        }
        return arrL;
    }
}
