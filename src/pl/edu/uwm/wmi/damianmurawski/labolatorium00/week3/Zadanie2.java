package pl.edu.uwm.wmi.damianmurawski.labolatorium00.week3;

import java.util.Random;
import java.util.Scanner;

public class Zadanie2 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        int n;
        System.out.println("Podaj n: ");
        n = input.nextInt();
        if(n<1 || n>=101){
            System.out.println("N = "+n+"\nN musi byc od 1 do 100");
            return ;
        }
        int[] array = new int[n];
        generate(array,n ,-999, 999);
        System.out.println("Ilość nieparzystch: "+ileNieparzystych(array,n));
        System.out.println("Ilość parzystch: "+ileParzystych(array,n));
        System.out.println("Ilość dodatnich: "+ileDodatnich(array,n));
        System.out.println("Ilość zer: "+ileZerowych(array,n));
        System.out.println("Ilość ujemnych: "+ileUjemnych(array,n));
        System.out.println("Ilość wystąpień maksymalnego elementu: "+ileMaksymalnych(array,n));
        System.out.println("Suma dodatnich: "+sumaDodatnich(array,n));
        System.out.println("Suma ujemnych: "+sumaUjemnych(array,n));
        System.out.println("Długość maksymalnego ciagu dodatnich: "+dlugoscMaksymalnegoCiaguDodatnich(array,n));
        signum(array,n);
        int lewy;
        int prawy;
        System.out.println("Podaj lewy: ");
        lewy = input.nextInt();
        if(1>lewy || lewy>=n){
            System.out.println("Lewy musi być od <1,"+n+")");
            return;
        }
        System.out.println("Podaj prawy: ");
        prawy = input.nextInt();
        if(1>prawy || prawy>=n){
            System.out.println("Prawy musi być od <1,"+n+")");
            return;
        }
        if(lewy>=prawy){
            System.out.println("Lewy musi być < Prawy");
            return;
        }
        odwrocFragment(array,n,lewy,prawy);
    }

    public static int getRandomNum(int min, int max) {
        Random random = new Random();
        return random.nextInt(max-min)+min;
    }
    public static void generate(int[] tab,int n, int min, int max) {
        for(int j=0; j<n; j++){
            tab[j] =getRandomNum(min, max);
            System.out.println(tab[j]);
        }
    }

    public static int ileNieparzystych(int[] tab,int n) {
        int nieparzyste =0;
        for(int i=0;i<n;i++){
            if(tab[i]%2!=0)
                nieparzyste++;
        }
        return nieparzyste;

    }
    public static int ileParzystych(int[] tab,int n) {
        int parzyste =0;
        for(int i=0;i<n;i++){
            if(tab[i]%2==0)
                parzyste++;
        }
        return parzyste;

    }
    public static int ileDodatnich(int[] tab,int n) {
        int dodatnie =0;
        for(int i=0;i<n;i++){
            if(tab[i]>0)
                dodatnie++;
        }
        return dodatnie;

    }
    public static int ileZerowych(int[] tab,int n) {
        int zerowe =0;
        for(int i=0;i<n;i++){
            if(tab[i]==0)
                zerowe++;
        }
        return zerowe;

    }
    public static int ileUjemnych(int[] tab,int n) {
        int ujemne =0;
        for(int i=0;i<n;i++){
            if(tab[i]<0)
                ujemne++;
        }
        return ujemne;

    }

    public static int ileMaksymalnych(int[] tab, int n) {
        int max=-999;
        int licznik =0;
        for(int i=0; i<n;i++){
            if(max<tab[i])
                max=tab[i];
        }
        for(int i=0; i<n;i++){
            if(max==tab[i])
                licznik++;
        }
        return licznik;

    }
    public static int sumaDodatnich(int[] tab , int n) {
        int sumaDodatnich = 0;
        for(int i=0;i<n;i++){
            if(tab[i]>0)
                sumaDodatnich += tab[i];

        }
        return sumaDodatnich;
    }
    public static int sumaUjemnych(int[] tab , int n) {
        int sumaUjemnych = 0;
        for(int i=0;i<n;i++){
            if(tab[i]<0)
                sumaUjemnych += tab[i];
        }
        return sumaUjemnych;
    }

    public static int dlugoscMaksymalnegoCiaguDodatnich(int[] tab, int n) {
        int iloscDodatnich = 0;
        int zmienna = 0;
        for(int i = 0; i<n; i++){
            if(tab[i]>0){
                zmienna++;
            }
            if(zmienna > iloscDodatnich){
                iloscDodatnich = zmienna;
            }
            if(tab[i]<0){
                zmienna = 0;
            }
        }
        return iloscDodatnich;
    }

    public static void signum(int[] tab, int n) {
        int[] tab2 = new int[n];
        System.out.println("\nTablica po zamianie(signum):");
        for(int i=0 ; i<n;i++){
            if(tab[i]<0)
                tab2[i]=-1;
            if(tab[i]>0)
                tab2[i]=1;
            System.out.println(tab2[i]);
        }

    }

    public static void odwrocFragment(int[] tab, int n, int lewy, int prawy) {
        int[] temp = new int[n];

        int k = 1;
        int j=0;
        System.out.println("Odwrócony fragment: ");
        for(int i = lewy; i<prawy ; i++){
            temp[j]=tab[prawy-k];
            System.out.print(" "+temp[j]);
            j++;
            k++;
        }
        System.out.println("\nPo odwróceniu: ");
        j =0;
        for(int i = lewy; i<prawy ; i++){
            tab[i]=temp[j];
            j++;
        }
        for(int i = 0 ; i<n;i++) {
            System.out.print(" " + tab[i]);
        }
    }
}
