package pl.edu.uwm.wmi.damianmurawski.labolatorium00.z11;
public class PairUtil<T> {
    public static <T> Pair<T> swap(Pair<T> object) {
        return new Pair<>(object.getSecond(), object.getFirst());
    }
}
