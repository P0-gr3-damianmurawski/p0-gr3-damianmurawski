package pl.imiajd.murawski.z08.zadanie3;

import java.time.LocalDate;

public class Flet extends  Instrument{
    @Override
    public void dzwiek() {
        System.out.println("Flet:fi, fi, fi;");
    }
    public Flet(String producent, LocalDate rokProdukcji){
        super(producent,rokProdukcji);
    }

}