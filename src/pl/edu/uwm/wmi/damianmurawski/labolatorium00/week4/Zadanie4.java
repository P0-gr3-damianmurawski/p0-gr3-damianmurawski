package pl.edu.uwm.wmi.damianmurawski.labolatorium00.week4;
import java.math.BigInteger;
import java.util.Scanner;
public class Zadanie4 {
    public static void main(String[] args) {
        System.out.println("Podaj n: ");
        Scanner input = new Scanner(System.in);
        int n = input.nextInt();
        BigInteger ilosc = new BigInteger("1");
        BigInteger suma = new BigInteger("0");
        ile(ilosc,suma,n);
    }

    public static void ile(BigInteger ilosc,BigInteger suma, int n) {
        int ilosc_pol = n * n;
        for(int i=1;i<ilosc_pol;i++){
            ilosc=ilosc.multiply(BigInteger.valueOf(2));
            suma = suma.add(ilosc);
        }
        suma = suma.add(BigInteger.valueOf(1));
        System.out.println(suma);
    }
}
