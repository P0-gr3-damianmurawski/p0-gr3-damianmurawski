package pl.edu.uwm.wmi.damianmurawski.labolatorium00.week5_2;

public class Zadanie2 {
    public static void main(String[] args) {
        IntegerSet s1 = new IntegerSet();
        for (int i = 0; i < 100; i++) {
            s1.set[i] = i > 2 && i <= 8;
        }
        s1.writeSet(s1.set);
        IntegerSet s2 = new IntegerSet();
        for (int i = 0; i < 100; i++) {
            s2.set[i] = i > 6 && i <= 14;
        }
        s2.writeSet(s2.set);
        IntegerSet.writeSet(IntegerSet.union(s1.set, s2.set));
        IntegerSet.writeSet(IntegerSet.intersection(s1.set, s2.set));
        IntegerSet.writeSet(IntegerSet.insertElement(30, s2.set));
        IntegerSet.writeSet(IntegerSet.deleteElement(7, s2.set));
        System.out.println(s1.toString());
        System.out.println(s1.equals(s2));
    }
}
class IntegerSet{
    boolean[] set = new boolean[100];
    public static void writeSet(boolean[] set) {
        for (int i = 0; i < 100; i++) {
            if (set[i])
                System.out.print(i + " , ");
        }
        System.out.println();
    }

    public static boolean[] union(boolean[] s1, boolean[] s2) {
        boolean[] us = new boolean[100];
        for (int i = 0; i < 100; i++) {
            us[i] = s1[i] || s2[i];
        }
        return us;
    }

    public static boolean[] intersection(boolean[] s1, boolean[] s2) {
        boolean[] temp = new boolean[100];
        for (int i = 0; i < 100; i++) {
            if (!s1[i] && !s2[i]) {
                temp[i] = false;
            }if (s1[i]==s2[i]){
                temp[i]=s2[i];
            }
        }
        return temp;
    }
    public static boolean[] insertElement(int liczba,boolean[]set){
        int i;
        for (i = 0; i <100; i++) {
            if(i == liczba){
                set[i] = true;
            }
        }
        return set;
    }
    public static boolean[] deleteElement(int liczba,boolean[]set){
        int i;
        for (i = 0; i <100; i++) {
            if(i == liczba){
                set[i] = false;
            }
        }
        return set;
    }
    public String toString(){
        StringBuilder str = new StringBuilder();
        for (int i = 0; i < 100; i++){
            if (set[i]){
                str.append(i).append(" ");
            }
        }
        return str.toString();

    }
    public boolean equals(Object obj) {
        IntegerSet other = (IntegerSet) obj;
        return this.set == other.set;
    }
}
