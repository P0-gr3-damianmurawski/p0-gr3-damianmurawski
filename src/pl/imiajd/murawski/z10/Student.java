package pl.imiajd.murawski.z10;

import java.time.LocalDate;

public class Student extends Osoba implements Cloneable,Comparable<Osoba>{
    private double sredniaOcen;
    public Student(double sredniaOcen, LocalDate dataUrodzenia,String nazwisko){
        super(nazwisko,dataUrodzenia);
        this.sredniaOcen=sredniaOcen;
    }
    public int compareTo(Student obj){
        int wynik;
        if(super.compareTo(obj)==0){
            if(this.sredniaOcen<obj.sredniaOcen)
                return -1;
            if(obj.sredniaOcen<this.sredniaOcen)
                return 1;
            return 0;
        }
        wynik = super.compareTo(obj);
        return wynik;
    }
    @Override
    public String toString(){
        return super.toString()+" średnia: "+sredniaOcen;
    }
}
