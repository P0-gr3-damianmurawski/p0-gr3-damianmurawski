package pl.edu.uwm.wmi.damianmurawski.labolatorium00.z13;

import java.util.PriorityQueue;
import java.util.Queue;
import java.util.Scanner;

public class Zadanie1 {
    private static final Queue<Zadanie> priorityQueue = new PriorityQueue<>();

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        while (true) {
            System.out.print("Wybierz jedno:\n'dodaj','nastepne','zakoncz'\n");
            String zadanie1 = scanner.nextLine();
            String[] podzielone = zadanie1.split(" ");
            if (podzielone[0].equals("dodaj")) {
                priorityQueue.add(new Zadanie(podzielone[1], podzielone[2]));
                wyswietlZadania();
            }
            if (zadanie1.equals("zakoncz")) {
                break;
            }
            if (zadanie1.equals("nastepne")) {
                priorityQueue.remove();
                wyswietlZadania();
            }
        }
    }
    private static void wyswietlZadania() {
        System.out.println("LISTA:");
        for (Zadanie i : priorityQueue) {
            System.out.println(i.getProrytet() + " " + i.getOpis());
        }
        System.out.println();
    }
    static class Zadanie implements Comparable<Zadanie> {
        public Zadanie(String priorytet, String opis) {
            this.priorytet = priorytet;
            this.opis = opis;
        }
        private final String priorytet;
        private final String opis;
        @Override
        public int compareTo(Zadanie o) {
            return this.priorytet.compareTo(o.priorytet);
        }
        public String getProrytet() {
            return priorytet;
        }
        public String getOpis() {
            return opis;
        }
    }

}
