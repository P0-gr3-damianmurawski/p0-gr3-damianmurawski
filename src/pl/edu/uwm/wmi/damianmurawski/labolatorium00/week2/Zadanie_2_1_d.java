package pl.edu.uwm.wmi.damianmurawski.labolatorium00.week2;
import java.util.Scanner;
import java.lang.Math;
public class Zadanie_2_1_d {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        int n;
        int wynik=0;
        System.out.println("Podaj n: ");
        n = input.nextInt();
        int liczba[];
        liczba = new int[n];
        for(int i=0; i<n ; i++){
            System.out.println("Podaj kolejna liczbe: ");
            liczba[i] = input.nextInt();
        }
        System.out.println("\n");
        for(int i=2; i<n-1 ; i++) {
            if(liczba[i]<(liczba[i-1]+liczba[i+1])/2)
                wynik++;
        }
        System.out.println(wynik);
    }
}

