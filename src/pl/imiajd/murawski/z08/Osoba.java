package pl.imiajd.murawski.z08;

import java.time.LocalDate;

public abstract class Osoba
{
    public Osoba(String nazwisko, String [] imiona, boolean plec, int year, int month, int day)
    {
        this.nazwisko = nazwisko;
        this.imiona = imiona;
        this.plec=plec;
        this.dataUrodzenia= LocalDate.of(year,month,day);
    }
    private String nazwisko;
    private String [] imiona;
    private boolean plec;
    private LocalDate dataUrodzenia;

    public abstract String getOpis();


    public String getNazwisko()
    {
        return nazwisko;
    }
    public LocalDate getDataUrodzenia()
    {
        return dataUrodzenia;
    }
    public String getImiona()
    {
        return imiona[0];
    }
    public String getplec()
    {
        if (plec == true){
            return "Kobieta";
        }else{
            return "Mężczyzna";
        }
    }
}
