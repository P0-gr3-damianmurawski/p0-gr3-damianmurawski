package pl.edu.uwm.wmi.damianmurawski.labolatorium00;

public class Zadanie4 {
    public static void main(String[] args) {
        int poczatek = 1000;
        double rok1 = poczatek *(1.06);
        double rok2 = rok1 * (1.06);
        double rok3 = rok2 * (1.06);
        System.out.println("Saldo "+ poczatek+" złotych");
        System.out.println("Saldo po pierwszym roku "+ rok1+" złotych");
        System.out.println("Saldo po drugim roku "+ rok2+" złotych");
        System.out.println("Saldo po trzecim roku "+ rok3+" złotych");
    }
}
