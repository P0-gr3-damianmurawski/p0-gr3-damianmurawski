package pl.edu.uwm.wmi.damianmurawski.labolatorium00.week2;
import java.util.Scanner;
import java.lang.Math;
public class Zadanie_2_1_h {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        int n;
        int wynik = 0;
        System.out.println("Podaj n: ");
        n = input.nextInt();
        double liczba[];
        liczba = new double[n];
        for (int i = 0; i < n; i++) {
            System.out.println("Podaj liczbe: ");
            liczba[i] = input.nextDouble();
        }
        System.out.println("\n");
        for (int i = 0; i < n; i++) {
            if (Math.abs(liczba[i]) < Math.pow(i, 2))
                wynik++;
        }

        System.out.println(wynik);
    }
}
