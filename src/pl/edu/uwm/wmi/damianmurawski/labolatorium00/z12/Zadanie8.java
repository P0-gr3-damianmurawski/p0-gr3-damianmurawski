package pl.edu.uwm.wmi.damianmurawski.labolatorium00.z12;

import java.util.LinkedList;

public class Zadanie8 {
    public static void main(String[] args){
        LinkedList<String> pracownicy = new LinkedList<>();
        pracownicy.add("Kowalski");
        pracownicy.add("Nowak");
        pracownicy.add("Wisniewski");
        pracownicy.add("Drozda");
        pracownicy.add("Piotrowski");
        pracownicy.add("Ptaszkiewicz");
        for(String i:pracownicy){
            System.out.println(i);
        }
        System.out.println("\n");
        print(pracownicy);
    }
    public static <E extends Iterable<?>> void print(E arg){
        for (Object i : arg) {
            System.out.print(i + ", ");
        }

    }
}
