package pl.imiajd.murawski.z10;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Collections;

public class TestStudent {
    public static void main(String[] args) {
        ArrayList<Osoba> grupa = new ArrayList<>();
        grupa.add(new Student(3.50, LocalDate.of(1999,10,1),"Kowalski"));
        grupa.add(new Student(3.60, LocalDate.of(1999,10,1),"Kowalski"));
        grupa.add(new Student(3.50, LocalDate.of(1999,12,1),"Nowak"));
        grupa.add(new Student(3.50, LocalDate.of(1999,12,1),"Brzęczyszczykiewicz"));
        grupa.add(new Student(3.50, LocalDate.of(1999,10,1),"Murawski"));
        for (Osoba i : grupa){
            System.out.println(i);
        }
        Collections.sort(grupa);
        System.out.println("\nPo sortowaniu:\n");
        for (Osoba i : grupa) {
            System.out.println(i);
        }
    }
}
