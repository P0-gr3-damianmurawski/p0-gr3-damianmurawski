package pl.edu.uwm.wmi.damianmurawski.labolatorium00.z12;

import java.util.Stack;

public class Zadanie6 {
    public static void main(String[] args) {
        podziel(2137);
    }
    public static void podziel(int liczba){
        Stack<Integer> stos = new Stack<>();
        while(liczba > 0){
            stos.push(liczba % 10);
            liczba = liczba/10;
        }
        while(!stos.empty()) {
            System.out.print(stos.pop()+"   ");
        }
    }
}
