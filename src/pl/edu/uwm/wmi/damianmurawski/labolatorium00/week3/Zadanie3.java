package pl.edu.uwm.wmi.damianmurawski.labolatorium00.week3;

import java.util.Random;
import java.util.Scanner;

public class Zadanie3 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        int m;
        int n;
        int k;
        System.out.println("Podaj m: ");
        m = input.nextInt();
        System.out.println("Podaj n: ");
        n = input.nextInt();
        System.out.println("Podaj k: ");
        k = input.nextInt();
        if(!(n>=1 && n<=10)){
            System.out.println("n = "+n+"\nN musi byc od 1 do 10");
            return ;
        }
        if(!(m>=1 && m<=10)){
            System.out.println("m = "+m+"\nN musi byc od 1 do 10");
            return ;
        }
        if(!(k>=1 && k<=10)){
            System.out.println("k = "+k+"\nN musi byc od 1 do 10");
            return ;
        }
        int min = 1;
        int max = 10;
        int i ;
        int j ;
        Random random = new Random();
        int[][] macierzA = new int[m][n];
        System.out.println("MACIERZ A MxN: ");
        for(i = 0;i<m;i++){
            for(j = 0;j<n;j++){
                macierzA[i][j] = (random.nextInt(max - min )+min);
                System.out.print(macierzA[i][j]+"  ");
            }
            System.out.println();
        }

        int[][] macierzB = new int [n][k];
        System.out.println("MACIERZ B NxK: ");
        for(i = 0;i<n;i++){
            for(j = 0;j<k;j++){
                macierzB[i][j] = (random.nextInt(max - min )+min);
                System.out.print(macierzB[i][j]+"  ");
            }
            System.out.println();
        }
        System.out.println("MACIERZ C MxK: ");
        int[][] macierzC = new int[m][k];
        for(i = 0;i<n;i++){
            for(j = 0;j<k;j++){
                for(int z=0; z<m; z++) {
                    macierzC[j][z]+=macierzA[z][i]*macierzB[i][j];
                }
            }

        }
        for(j=0;j<m;j++){
            for(i=0;i<k;i++){
                System.out.print(macierzC[i][j] + "  ");
            }
            System.out.println();
        }

    }
}
