package pl.imiajd.murawski.z10;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Collections;

public class TestOsoba {
    public static void main(String[] args) {
        ArrayList<Osoba> grupa = new ArrayList<>();
        grupa.add(new Osoba("Kowalski", LocalDate.of(1999,10,1)));
        grupa.add(new Osoba("Kowalski", LocalDate.of(1999,11,1)));
        grupa.add(new Osoba("Nowak", LocalDate.of(1999,12,1)));
        grupa.add(new Osoba("Brzęczyszczykiewicz", LocalDate.of(1999,12,1)));
        grupa.add(new Osoba("Murawski", LocalDate.of(1999,10,1)));
        for (Osoba i : grupa){
            System.out.println(i);
        }
        Collections.sort(grupa);
        System.out.println("\nPo sortowaniu:\n");
        for (Osoba i : grupa){
            System.out.println(i);
        }
        System.out.println(grupa.get(1).equals(grupa.get(2)));
        System.out.println(grupa.get(1).compareTo(grupa.get(4)));
    }
}
