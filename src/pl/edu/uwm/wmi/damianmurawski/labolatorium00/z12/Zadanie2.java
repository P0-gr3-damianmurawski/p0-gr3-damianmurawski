package pl.edu.uwm.wmi.damianmurawski.labolatorium00.z12;

import java.util.LinkedList;

public class Zadanie2 {
    public static void main(String[] args) {
        LinkedList<String> pracownicy = new LinkedList<>();
        pracownicy.add("Kowalski");
        pracownicy.add("Nowak");
        pracownicy.add("Wisniewski");
        pracownicy.add("Drozda");
        pracownicy.add("Piotrowski");
        pracownicy.add("Ptaszkiewicz");
        for(String i:pracownicy){
            System.out.println(i);
        }
        redukuj(pracownicy,2);
        System.out.println("\nREDUKUJ:\n");
        for(String i:pracownicy){
            System.out.println(i);
        }
    }
    public static<T> void redukuj(LinkedList<T>pracownicy,int n){
        for(int i = n - 1; i < pracownicy.size(); i += n - 1) {
            pracownicy.remove(i);
        }
    }
}
